import { Contract, ethers, Signer } from "ethers";
import params from '../config/params';
import { IChainFactory } from "./interfaces/IChainFactory";

export class EvmFactory implements IChainFactory {
    public chain;

    constructor(chain: string) {
        this.chain = params.chains[chain];
    }

    getProvider() {
        const provider = new ethers.providers.JsonRpcProvider(
            this.chain.providerUrl,
            {name: this.chain.name, chainId: this.chain.chainId}
        );
        return provider;
    }

    getRouterContract(signer: Signer): Contract {
        return new ethers.Contract(this.chain.router, params.routerAbi, signer);
    }

    getTokenContract(address: string, signer: Signer): Contract {
        return new ethers.Contract(address, params.tokenAbi, signer);
    }

    async getGasPrice() {
        const gasPrice = await this.getProvider().getGasPrice();
        return gasPrice;
    }

    async getLastBlockNumber() {
        const block = await this.getProvider().getBlockNumber();
        return block;
    }
}