import {L0Bridge} from './l0bridge';

const [,, script, ...params] = process.argv;

switch (script) {
    case 'L0Bridge':
        const init = new L0Bridge();
        await init.run(...params);
        break;
}
