export interface IChain {
    chainId: number;
    name: string;
    baseToken: string,
    providerUrl: string;
    explorerUrl: string;
    router: string;
    routerChainId: number;
    tokens: Record<string, string>
}