import { IChain } from "./IChain";

export interface IChainFactory {
    chain: IChain;
    getProvider(): any;
    getRouterContract(signer): any;
    getTokenContract(address, signer): any;
    getGasPrice(): any;
    getLastBlockNumber(): any;
}