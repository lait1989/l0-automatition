import { EvmFactory } from "./evmFactory";
import { WalletInfo } from "./walletInfo";
import { BigNumber, ethers, Signer } from "ethers";
import { IChainFactory } from "./interfaces/IChainFactory";
import params from '../config/params';

export class L0Bridge {
    async run(args: string[]) {
        try {
            const [privateKey, chainFrom, chainTo, tokenFrom, tokenTo, value] = args;

            const chainFromFactory = new EvmFactory(chainFrom);
            const signer = WalletInfo.getSigner(privateKey, chainFromFactory);
            const address = await signer.getAddress();

            const walletTokenBaseBalance = await WalletInfo.getBalance(chainFromFactory.chain.baseToken, chainFromFactory, signer);
            if (Number(walletTokenBaseBalance) === 0) {
                throw new Error('Количество базовых токенов для отправки не может быть пустым');
            }

            const walletTokenBalance = await WalletInfo.getBalance(params.chains[chainFrom]['tokens'][tokenFrom], chainFromFactory, signer);
            if (Number(walletTokenBalance) === 0) {
                throw new Error('Количество токенов для отправки не может быть пустым');
            }

            const tokenFromContract = await chainFromFactory.getTokenContract(params.chains[chainFrom]['tokens'][tokenFrom], signer);
            const tokenFromDecimals = await tokenFromContract.decimals();

            const amount = ethers.utils.parseUnits(value.toString(), tokenFromDecimals);
            const amountWithSlippage = amount.sub(amount.mul(5).div(1000));

            await this.allowTokenForRouter(params.chains[chainFrom]['tokens'][tokenFrom], amount, chainFromFactory, signer);
            const chainToFactory = new EvmFactory(chainTo);

            const routerContract = await chainFromFactory.getRouterContract(signer);
            const swapFee = await routerContract.quoteLayerZeroFee(
                chainToFactory.chain.routerChainId,
                1, // function type swap
                address,
                "0x",
                [0,
                0,
                "0x"
                ]
            );

            const gasPrice = await chainFromFactory.getGasPrice();
            const nonce = await WalletInfo.getTransactionCount(address, chainFromFactory) + 1;

            const estimateGasRaw = await routerContract.estimateGas.swap(
                chainToFactory.chain.routerChainId,
                params.pools[tokenFrom],
                1,
                address,
                amount,
                amountWithSlippage,
                { dstGasForCall: 0, dstNativeAmount: 0, dstNativeAddr: "0x" },
                address,
                "0x",
                {from: address, value: swapFee[0]}
            );

            const swapTxRaw = await routerContract.swap(
                chainToFactory.chain.routerChainId,
                params.pools[tokenTo],
                1,
                address,
                amount,
                amountWithSlippage,
                { dstGasForCall: 0, dstNativeAmount: 0, dstNativeAddr: "0x" },
                address,
                "0x",
                {from: address, value: swapFee[0], gasLimit: estimateGasRaw, gasPrice, nonce}
            );
            const swapTx = await swapTxRaw.wait();
            console.log(`Transaction swap: ${chainFromFactory.chain.explorerUrl}/tx/${swapTx.transactionHash}`);
        } catch (e) {
            console.log(e.message);
        }

    }

    private async allowTokenForRouter(token: string, value: BigNumber, chainFromFactory: IChainFactory, signer: Signer) {
        const address = await signer.getAddress();
        const tokenContract = await chainFromFactory.getTokenContract(token, signer);
        const allowanceToken = await tokenContract.allowance(address, chainFromFactory.chain.router);

        if (Number(allowanceToken['_hex']) === 0 || Number(allowanceToken['_hex']) < Number(value)) {
            const gasPrice = await chainFromFactory.getGasPrice();
            const tokenApproveTxRaw = await tokenContract.approve(
                chainFromFactory.chain.router,
                value,
                {gasPrice}
            );
            const tokenApproveTx = await tokenApproveTxRaw.wait();
            console.log(`Transaction approve: ${chainFromFactory.chain.explorerUrl}/tx/${tokenApproveTx.transactionHash}`);
        }
    }
}