import { IChainFactory } from "./interfaces/IChainFactory";
import { ethers, Signer } from "ethers";

export class WalletInfo {
    public static getSigner(privateKey: string, chainFactory: IChainFactory): Signer {
        const provider = chainFactory.getProvider();
        const walletProvided = new ethers.Wallet(privateKey, provider);
        return walletProvided.connect(provider);
    }

    public static async getTransactionCount(
        address: string,
        chainFactory: IChainFactory,
    ) {
        return chainFactory.getProvider().getTransactionCount(address, 'latest');
    }

    public static async getBalance(
        token: string,
        chainFactory: IChainFactory,
        signer: Signer
    ) {
        const address = await signer.getAddress();
        let balanceRaw = 0;
        if (token === chainFactory.chain.baseToken) {
            balanceRaw = await chainFactory.getProvider().getBalance(address);
        } else {
            const providedContract = chainFactory.getTokenContract(
                token,
                signer,
            );
            balanceRaw = await providedContract.balanceOf(address);
        }
        return balanceRaw;
    }
}